﻿using Microsoft.Office.Interop.Excel;
using System.Collections.Generic;
using System.IO;

namespace LabaNumberTwo
{
    public class Excel
    {
        string path = "";
        public static Application excelApp = new Application();
        static Workbook wb;
        static Worksheet ws;
        public Excel(string path, int Sheet)
        {
            this.path = path;
            wb = excelApp.Workbooks.Open(path);
            ws = excelApp.Worksheets[Sheet];
        }

        public string ReadCell(int i, int j)
        {
            i++;
            j++;
            if (ws.Cells[i, j].Value2 != null)
            {
                return ws.Cells[i, j].Value2.ToString();
            }
            else
            {
                return "";
            }

        }
        public void Close()
        {
            wb.Close(0);
            excelApp.Quit();
        }
        public static void TableCreate()
        {
            int counter = 2;
            while (true)
            {
                var row = new Row(counter);
                if (row.IsEmpty == true)
                {
                    break;
                }
                counter++;
            }
            Row.excel.Close();
            if ((Row.rows.Count) % (28) == 0) MainWindow.pagesCount = (Row.rows.Count) / (28);
            else
            {
                MainWindow.pagesCount = ((Row.rows.Count) / (28)) + 1;
                MainWindow.appendix = (Row.rows.Count) % (28);
            }
            Row.logs = Row.rows;
        }
        public static void WriteFie(List<Row> list)
        {
            Application tmpexcelApp = new Application();
            Workbook tmpwb = tmpexcelApp.Workbooks.Add();
            Worksheet tmpws = tmpwb.Worksheets[1];
            tmpws.Cells[1, 1].Value2 = "Идентификатор угрозы";
            tmpws.Cells[1, 2].Value2 = "Наименование угрозы";
            tmpws.Cells[1, 3].Value2 = "Описание угрозы";
            tmpws.Cells[1, 4].Value2 = "Источник угрозы";
            tmpws.Cells[1, 5].Value2 = "Объект воздействия угрозы";
            tmpws.Cells[1, 6].Value2 = "Нарушение конфиденциальности";
            tmpws.Cells[1, 7].Value2 = "Нарушение целостности";
            tmpws.Cells[1, 8].Value2 = "Нарушение доступности";
            for (int i = 2; i < list.Count+2; i++)
            {
                    tmpws.Cells[i, 1].Value2 = list[i-2].Id;
                    tmpws.Cells[i, 2].Value2 = list[i - 2].Name;
                    tmpws.Cells[i, 3].Value2 = list[i - 2].Description;
                    tmpws.Cells[i, 4].Value2 = list[i - 2].Source;
                    tmpws.Cells[i, 5].Value2 = list[i - 2].Target;
                    tmpws.Cells[i, 6].Value2 = list[i - 2].Privacy;
                    tmpws.Cells[i, 7].Value2 = list[i - 2].Consistency;
                    tmpws.Cells[i, 8].Value2 = list[i - 2].Accessibility;

            }
            tmpwb.SaveAs(Directory.GetCurrentDirectory()+"\\generated.xlsx");
            
            tmpwb.Close(0);
            tmpexcelApp.Quit();
        }
    }
}
