﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Windows;
using System.Windows.Controls;

namespace LabaNumberTwo
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        static List<Row> currentPage = new List<Row>();
        public static int pagesCount;
        public static int appendix;

        public MainWindow()
        {
            InitializeComponent();
            btnDownload.IsEnabled = false;
            btnPrevious.IsEnabled = false;
            btnNext.IsEnabled = false;
            btnSave.IsEnabled = false;
            btnLog.IsEnabled = false;
        }
        public static int current = 1;
        private void Button_Click(object sender, RoutedEventArgs e)
        {

            try
            {
                Row.excel = new Excel(Row.path, 1);
                Excel.TableCreate();

                btnPrevious.IsEnabled = true;
                btnNext.IsEnabled = true;
                btnSave.IsEnabled = true;
                btnOpen.IsEnabled = false;
                
                for (int i = 0; i < 30; i++)
                {
                    currentPage.Add(Row.rows[i]);
                }
                Datazz.ItemsSource = currentPage;
            }
            catch (Exception)
            {
                MessageBox.Show("Не удалось найти базу данных в текущей папке. Нажмите кнопку \"Скачать\" чтобы исправить это");
                btnDownload.IsEnabled = true;
            }

        }
        private void btnNext_Click(object sender, RoutedEventArgs e)
        {
            current++;
            PageFinder(current);
            Datazz.Items.Refresh();
        }

        private void btnPrevious_Click(object sender, RoutedEventArgs e)
        {
            current--;
            PageFinder(current);
            Datazz.Items.Refresh();
        }
        private static void PageFinder(int number)
        {
            if (number > pagesCount) current = pagesCount;
            if (number < 1) current = 1;
            number = current;
            if (appendix == 0)
            {
                currentPage.Clear();
                for (int i = 0; i < 28; i++)
                {
                    currentPage.Add(Row.rows[i + (number - 1) * 28]);
                }
            }
            else if (number == pagesCount)
            {
                currentPage.Clear();
                for (int i = 0; i < appendix; i++)
                {
                    currentPage.Add(Row.rows[i + (number - 1) * 28]);
                }
            }
            else
            {
                currentPage.Clear();
                for (int i = 0; i < 28; i++)
                {
                    currentPage.Add(Row.rows[i + (number - 1) * 28]);
                }
            }
        }

        private void Datazz_CellEditEnding(object sender, DataGridCellEditEndingEventArgs e)
        {
            
        }

        private void btnDownload_Click(object sender, RoutedEventArgs e)
        {
            string link = ("https://bdu.fstec.ru/files/documents/thrlist.xlsx");
            WebClient downloader = new WebClient();
            downloader.DownloadFile(link, "thrlist.xlsx");
            btnDownload.IsEnabled = false;
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Excel.WriteFie(Row.rows);
            }
            catch (Exception x)
            {
                MessageBox.Show(x.Message.ToString()+"darov");
            }
        }

        private void btnLog_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Datazz_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            this.EditNote();

            this.Show();
            Datazz.Items.Refresh();
        }
         void EditNote()
        {
            int selectedColumn = Datazz.CurrentCell.Column.DisplayIndex;
            var selectedCell = Datazz.SelectedCells[0];
            var cellContent = selectedCell.Column.GetCellContent(selectedCell.Item);
            int number = 0;
            Row chosenOne = null;
            for (int i = 0; i < Row.rows.Count; i++)
            {
                if (Row.rows[i].ShortForm == (cellContent as TextBlock).Text)
                {
                    number = i;
                    chosenOne = Row.rows[i];
                    break;
                }
            }
            Logs window = new Logs(number);
            window.Show();
            this.Hide();
            var tmpList = new List<Row>();
            tmpList.Add(chosenOne);
            window.gridChange.ItemsSource = tmpList;
            Row.rows[number] = chosenOne;

        }

    }

}
