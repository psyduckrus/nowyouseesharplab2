﻿using System.Collections.Generic;
using System.IO;

namespace LabaNumberTwo
{
    public class Row
    {
        public static List<Row> rows = new List<Row>();
        public static List<Row> logs = new List<Row>();
        public static string path = (Directory.GetCurrentDirectory()+"\\thrlist.xlsx");
        public static Excel excel;
        private int counter = 0;
        public bool IsEmpty = false;
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Source { get; set; }
        public string Target { get; set; }
        public string ShortForm
        {
            get
            {
                return ($"УБИ.{Id}");
            }
            set
            {
                
            }
        }
        public string Privacy
        {
            get
            {
                return this.privacy;
            }
            set
            {
                if (value == "1") privacy = "да";
                else privacy = "нет";
            }
        }
        public string Consistency
        {
            get
            {
                return this.consistency;
            }
            set
            {
                if (value == "1") consistency = "да";
                else consistency = "нет";
            }
        }
        public string Accessibility
        {
            get
            {
                return this.accessibility;
            }
            set
            {
                if (value == "1") accessibility = "да";
                else accessibility = "нет";
            }
        }
        private string privacy, consistency, accessibility;

        public Row(int row)
        {
            
                for (int i = 0; i < 8; i++)
                {
                    if ((excel.ReadCell(row, i) == ""))
                    {
                        this.counter++;
                    }
                    if (i == 0) this.Id = excel.ReadCell(row, i);
                    else if (i == 1) this.Name = excel.ReadCell(row, i);
                    else if (i == 2) this.Description = excel.ReadCell(row, i);
                    else if (i == 3) this.Source = excel.ReadCell(row, i);
                    else if (i == 4) this.Target = excel.ReadCell(row, i);
                    else if (i == 5) this.Privacy = excel.ReadCell(row, i);
                    else if (i == 6) this.Consistency = excel.ReadCell(row, i);
                    else if (i == 7) this.Accessibility = excel.ReadCell(row, i);
                }
                if (this.counter != 8)
                {
                    rows.Add(this);
                }
                else
                {
                    this.IsEmpty = true;
                }
            

        }
    }
}
